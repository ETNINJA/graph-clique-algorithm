/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otimiz;

import java.util.Vector;

public class Node {
    
    private Double value;
    private int degree;
    private Vector<Integer> neibourgh;

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public int getDegree() {
        return degree;
    }

    public void setDegree(int degree) {
        this.degree = degree;
    }

    public int getNeibourgh(int i) {
        return neibourgh.get(i);
    }
    
    public void setNeibourgh(Vector<Integer> neibourgh) {
        this.neibourgh = neibourgh;        
    }
    
    public boolean contains (int i){
        if(neibourgh.contains(i))
            return true;
        return false;       
    }
    
    public void printNeibourgh()
    {
        neibourgh.forEach(result -> System.out.print(result + " "));
        
    }
    public void printNode()
    {
        System.out.println("Value: "+ value);
        System.out.println("Degree: "+ degree);
        printNeibourgh();
    }

}