/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otimiz;

import java.util.Scanner;
import java.util.Vector;

public class Graph {

    private int vertex;
    private int edges;
    private Vector<Node> nodes = new Vector<Node>();

    public int getVertex() {
        return vertex;
    }

    public void setVertex(int vertex) {
        this.vertex = vertex;
    }

    public int getEdges() {
        return edges;
    }

    public void setEdges(int edges) {
        this.edges = edges;
    }

    public Vector<Node> getNodes() {
        return nodes;
    }

    public void setNodes(Vector<Node> nodes) {
        this.nodes = nodes;
    }

    public void load(Scanner input) {
        for (int j = 0; j < vertex; j++) {
            String entrada = input.nextLine();
            String[] strArray = entrada.split("\\s+");
            Node N = new Node();
            N.setValue(Double.parseDouble(strArray[0]) / Double.parseDouble(strArray[1]));
            N.setDegree(Integer.parseInt(strArray[2]));
            Vector<Integer> aux = new Vector<Integer>();
            for (int i = 3; i < strArray.length; i++) {
                aux.add(Integer.parseInt(strArray[i]));
            }
            N.setNeibourgh(aux);
            nodes.add(N);
        }
    }

    public void printGraph() {
        System.out.println("Vertex: " + vertex);
        System.out.println("Edges: " + edges);
        for (Node N : nodes) {
            N.printNode();
        }
    }

    public Vector<Integer> findClick() {
        Vector<Integer> click = new Vector<Integer>();
        for (int i = 0; i < vertex; i++) {
            click.clear();
            System.out.println("\n -> Vertex: " + i);
            click.add(i);
            click.add(nodes.get(i).getNeibourgh(0));//add the first neibourgh
            for (int j = 1; j < nodes.get(i).getDegree(); j++) {
                int checkNeibough = nodes.get(i).getNeibourgh(j);               
                Vector<Integer> aux = new Vector<Integer>();
                for (int z = 0; z < click.size(); z++) {
                    if (nodes.get(checkNeibough).contains(click.get(z))) {
                        aux.add(click.get(z));
                    }
                }
                if (click.equals(aux)) {
                    click.add(checkNeibough);
                }
            }
            System.out.println("Clique elements: ");
            click.forEach(result -> System.out.print(result + " "));
            if (click.size() > 2) {
                break;
            }

        }
        return click;
    }

    public void bounds(Vector<Integer> click) {
        Vector<Double> bound = new Vector<Double>();
        click.forEach(result -> bound.add(nodes.get(result).getValue()));
        System.out.println("\nClique Values: ");
        bound.forEach(result -> System.out.print(result + " "));
        System.out.println("\n Min Bound: " + bound.get(getMinValue(bound))
                + " -> vertex: " + click.get(getMinValue(bound)));
        System.out.println(" Max Bound: " + bound.get(getMaxValue(bound))
                + " -> vertex: " + click.get(getMaxValue(bound)));

    }

    public int getMinValue(Vector<Double> bound) {
        int minValue = 0;
        for (int i = 1; i < bound.size(); i++) {
            if (bound.get(i) < bound.get(minValue)) {
                minValue = i;
            }
        }
        return minValue;
    }

    public int getMaxValue(Vector<Double> bound) {
        int maxValue = 0;
        for (int i = 1; i < bound.size(); i++) {
            if (bound.get(i) > bound.get(maxValue)) {
                maxValue = i;
            }
        }
        return maxValue;
    }
}
